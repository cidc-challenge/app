// app.test.js
const fs = require('fs');

describe('Archivo index', () => {
  it('Debe existir', () => {
    const path = './src/index.js';
    const existe = fs.existsSync(path);
    expect(existe).toBe(true);
  });
});

describe('Archivo test', () => {
    it('Debe existir', () => {
      const path = './test/app.test.js';
      const existe = fs.existsSync(path);
      expect(existe).toBe(true);
    });
  });

  